﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingWall : MonoBehaviour
{
    //determines if the wall is supposed to move or not
    public bool moving;

    bool up = true;


    // Update is called once per frame
    void Update()
    {
        MoveWall();
    }

    //moves the wall up or down
    public void MoveWall()
    {
        //will move the wall up as long as it is a moving wall that is going
        //up and it is not the same height as the other walls,
        //otherwise sets the wall to go down.
        if (moving && up && transform.position.y < 0.5)
            transform.Translate(Vector3.up * Time.deltaTime, Space.World);
        else
            up = false;

        //will move the wall down as long as it is a moving wall that is
        //going down and it is not flush with the floor,
        //otherwise sets the wall to go up.
        if (moving && !up && transform.position.y > -0.5)
            transform.Translate(Vector3.down * Time.deltaTime, Space.World);
        else
            up = true;
    }

    //turns the wall 90 degrees when called
    public void Turn()
    {
        //will rotate if is not a moving wall and if has its original rotation
        if (!moving && transform.rotation.y == 0)
            transform.rotation = Quaternion.Euler(new Vector3(0, 90, 0));
        //resets back to orginal rotation if again not a moving wall
        else if(!moving )
            transform.rotation = Quaternion.Euler(new Vector3(0, 0, 0));
    }
}
