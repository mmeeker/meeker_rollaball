﻿using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed;
    public Text countText;
    public Ending end;
    public Slider HealthBar; 
    public float time;

    bool gameEnd = false;
    float health;
    Rigidbody rb;

    /*
     * reference to the Trail component of the player.
     * the trail has Autodestruct that destroys the 
     * object it is attached to if there is no trail
     * and this is turned on and simulates the losing 
     * condition.
     * the trail's length is determined by how long 
     * it exists, so by decreasing this time the 
     * length of the trail is shortened.
     */
    TrailRenderer tr;

    int count;
    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        tr = GetComponent<TrailRenderer>();
        time = 3.5f;
        health = time * 10;
        HealthBar.maxValue = health;
        HealthBar.value = health;       
        count = 13;      
        SetText();
     }

    private void Update()
    {
        if (!gameEnd)
        {
            time -= Time.deltaTime / 10;
            health = time * 10;
            HealthBar.value = health;
            checkHealth();
            //updates the trails time. +0.5 is to have a trail near time = 0
            tr.time = time + 0.5f;
            
        }
    }

    //if the player is destroyed it will call the 
    //gameover function saying you lost(false)
    private void checkHealth()
    {
        if (health <= 0 && !gameEnd)
        {
            gameEnd = true;
            this.gameObject.SetActive(false);
            end.GameOver(false);
        }

    }

    void FixedUpdate()
    {
        //sets the horizontal and vertical to the inputs
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVeritical = Input.GetAxis("Vertical");
        
        //moves the player according to the horizontal and vertical variables
        Vector3 movement = new Vector3(moveHorizontal, 0f, moveVeritical);
        rb.AddForce(movement * speed);

        //resets the players position back to the play area if they 
        //mangage to fall out of the play area.
        if (transform.position.y < 0)
            transform.position = new Vector3(0, 0, 0);        
    }

    void OnTriggerEnter(Collider other)
    {
        /*
         * if it collides with a pick up object it will
         * deactivate said objects, decrease the count of 
         * how many is left, increase health(time), updates
         * the count text, and calls the function that
         * rotates the walls
         */

        if (other.gameObject.CompareTag("Pick Up"))
        {
          
            other.gameObject.SetActive(false);
            count -= 1;
            time += 0.2f;
            SetText();

            MovingWall[] walls = FindObjectsOfType<MovingWall>();
            foreach (MovingWall aWall in walls)
                aWall.Turn();
        }
     }

    void SetText()
    {
        //shows how many more objects you need to pick up
        countText.text = "Objects left: " + count.ToString();

        //once there are no more objects to pick up it will
        //call the gameover function saying the you won(true)
        if (count <= 0 && !gameEnd)
        {
            gameEnd = true;
            end.GameOver(true);
        }
    }

    
}
