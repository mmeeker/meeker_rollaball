﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Ending : MonoBehaviour
{
    public Text endText;

    void Start()
    {
        //hides the ending text at the start
        endText.text = "";
    }

    //depending on the state sent it will provide the
    //appropriate ending text
    public void GameOver(bool gameState)
    {
        if (gameState)
            endText.text = "You Win!";
        else
            endText.text = "You Lose!";

    }
}

